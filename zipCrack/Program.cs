﻿using Ionic.Zip;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zipCrack.Properties;

namespace zipCrack
{
    class Program
    {
        static void Main(string[] args)
        {

            string zipPath = args[0];
            //string zipPath = Path.GetFullPath(@"C:\Users\Anders\source\repos\sparetimeprojects\zipCrack\bin\Debug\ele124-oppgavesett og vedlegg.zip");
            Console.WriteLine($"Zipfile:\t{zipPath}");
            string fileName = Path.GetFileName(zipPath);
            string extractPath = zipPath.Substring(0,zipPath.Length-fileName.Length)+@"extracted\";
            Console.WriteLine($"Extract to:\t{extractPath}");
            string[] seperator = {"\r\n"};
            string[] passList = Resources.simpleWords.Split(seperator, StringSplitOptions.None);
            string pass = FindPass(zipPath, passList);
            Console.WriteLine(pass);

            using (ZipFile archive = ZipFile.Read(zipPath))
            {
                archive.Encryption = EncryptionAlgorithm.PkzipWeak; // the default: you might need to select the proper value here
                archive.Password = pass;
                archive.ExtractAll(extractPath, ExtractExistingFileAction.Throw);
            }
        }

        private static string FindPass(string zipPath, string[] passList)
        {
            char currChar='0';
            //archive.StatusMessageTextWriter = Console.Out;
            foreach (string word in passList)
            {
                if (currChar!=Char.ToUpper(word[0]))
                {
                    currChar = Char.ToUpper(word[0]);
                    Console.Write($"\r Current Length: {word.Length} \t Current letter: {currChar} \t");
                }
                try
                {
                    if (ZipFile.CheckZipPassword(zipPath, word))
                    {
                        return word;
                    }
                }
                catch (BadPasswordException exBadPassword)
                {
                    //Console.WriteLine(exBadPassword.Message + " : " + word);
                }
                catch (BadCrcException badCRCex)
                {
                }
                catch (ZipException zipEx)
                {
                    //Console.Write(word + " Extracted password" + "\t" + zipEx.Message);
                }
                catch (FileNotFoundException fileNotFoundException)
                {
                    Console.WriteLine("File not found");
                    throw fileNotFoundException;
                }
                catch (Exception ex)
                {
                    //Console.Write(word + "\t" + ex.Message);
                }
            }
            return null;
        }
}
}
