﻿// ELE124 ex 16/12-2016 oppg2 - utgangspunkt

using System;

namespace oppg2
{
    class Program
    {
        // 2a: 

        public static void Main(string[] args)
        {
            try
            {
                string [] navnFlere = new string[] {"Knut", "Laila", "Synne", "Even", "Klara", "Edvin", "Alfred" };
                int[] heltallFlere = new int[] { 90, -17, 81, 0, 4, -3, 8 };
                
                // 2b:
 
                Console.WriteLine("Legger nye navn inn i en liste: ");
                // 2c i:
                VisListe(stringListe);

                // 2e:

                Console.WriteLine("Fjerner alle!");
                stringListe.FjernAlle();
                VisListe(stringListe);

                Console.WriteLine("Legger nye tall inn i en liste: ");
                // 2c ii:
                VisListe(intListe);

                Pause();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Feilmelding: " + ex.Message);
                Pause();
            }
        }

        private static void Pause()
        {
            Console.WriteLine("Trykk en tast ...");
            Console.ReadKey();
        }

        // 2d:

    }
}
