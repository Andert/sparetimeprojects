﻿// ELE124 ex 16/12-2016 oppg2 - utgangspunkt

using System;
using System.Collections;

namespace oppg2
{
    class Liste<T>
    {
        private ArrayList elementer;

        public Liste()
        {
            elementer = new ArrayList();
        }

        public void LeggTil(T innVerdi)
        {
            elementer.Add(innVerdi);
            elementer.Sort();
        }

        public void Fjern(T utVerdi)
        {
            if (elementer.Contains(utVerdi))
                elementer.Remove(utVerdi);
            else
            {
                if (elementer.Count < 1)
                    throw new FinnesIkkeException();
                else
                    throw new FinnesIkkeException(utVerdi.ToString());
            }
        }

        public void FjernAlle()
        {
            elementer.Clear();
        }
    }

    class FinnesIkkeException : Exception
    {
        public FinnesIkkeException()
            : base("Listen er tom!")
        {
        }

        public FinnesIkkeException(string exception)
            : base("Dette elementet finnes ikke i listen: " + exception)
        {
        }
    }
}
