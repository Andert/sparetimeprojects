﻿// Filnavn: oppg1d-start.cs

using System;

namespace oppg1d
{
    class Student
    {
    
        // Her mangler kode.

    }

    class Program
    {
        static void Main(string[] args)
        {
            Student a = new Student();
            a.Navn = "Bang, Turid";
            a.StudNr = "h123456";
            a.Epost = "Turid.Bang@stud.hib.no";
            a.Mobil = "+4788112288";

            Student b = new Student("Dahl, Kjell", "h123457", "Kjell.Dahl@hotmail.com");

            Console.WriteLine("Nye studenter: {0} og {1}", a.Navn, b.Navn);

            Console.WriteLine("---  Alle studenter  ---");
            Console.WriteLine(a);
            Console.WriteLine(b);
            Console.WriteLine("Antall studenter: {0}", Student.Antall);

            Console.WriteLine("Trykk en tast ...");
            Console.ReadKey();
        }
    }
}
