﻿using NotificationForm.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;

namespace NotificationForm
{
    public partial class NotificationComponent : Component
    {
        public string Location { get; set; }
        public bool Rain { get; private set; }
        public string ForecastString { get; private set; }
        public List<float> Forecast { get; private set; }
        public NotificationComponent(System.Threading.ManualResetEvent _quitEvent, string location)
        {
            this.Location = location;
            InitializeComponent();

            System.Timers.Timer timer = new System.Timers.Timer();
            timer.Interval = 10 * 60 * 1000; // 10 minutes
            timer.Elapsed += new ElapsedEventHandler(this.OnTimer);
            timer.Start();

            notifyIcon1.MouseMove += new MouseEventHandler(this.MouseMoved);
            notifyIcon1.MouseClick += new MouseEventHandler(this.MoouseClicked);
            MenuItem locationMenuItem = new MenuItem("MenuItem");
            locationMenuItem.Click += new EventHandler(SetLocationClick);
            notifyIcon1.ContextMenu = new ContextMenu(new MenuItem[]
            {
                new MenuItem("Set Location")
            });
            notifyIcon1.ContextMenu.MenuItems.Add(locationMenuItem);
            getForecast();
        }

        private void SetLocationClick(object sender, EventArgs e)
        {
            MessageBox.Show("Showing locations");
        }

        private void MoouseClicked(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                getForecast();
                notifyIcon1.BalloonTipText = ForecastString;
                notifyIcon1.ShowBalloonTip(5);
            }
            else if(e.Button == MouseButtons.Right)
            {
             //   notifyIcon1.ContextMenu.Show(,new System.Drawing.Point(0, 0));
            }
        }

        private void MouseMoved(object sender, MouseEventArgs e)
        {
        }

        private void OnTimer(object sender, ElapsedEventArgs e)
        {
            getForecast();
        }

        private void getForecast()
        {
            Forecast = ForecastFetcher.GetForecast(Location);
            ForecastString = ForecastFetcher.GetStringForecast(Location, Forecast);
            Rain = ForecastFetcher.IsRainingNext90Min(Location, Forecast);
            SetIcon();
        }

        private void SetIcon()
        {
            if (Rain)
            {
                notifyIcon1.Icon = Resources.blueIcon;
                notifyIcon1.Text = "Chances of Rain for next 90min";
            }
            else
            {
                notifyIcon1.Icon = Resources.greenIcon;
                notifyIcon1.Text = "No chance of Rain in next 90min";
            }
        }

        private void contextMenuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            MessageBox.Show("Showing locations");
        }
    }
}
