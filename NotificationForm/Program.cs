﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using NotificationForm.Properties;

namespace NotificationForm
{
    static class Program
    {
        static ManualResetEvent _quitEvent = new ManualResetEvent(false);

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Places places = new Places();
            string bergenLocation = "Hordaland/Bergen/Bergen/";
            string sandnessjøenLocation = "Nordland/Alstahaug/Sandnessjøen/";
            NotificationComponent notificationComponent = new NotificationComponent(_quitEvent, sandnessjøenLocation);



            _quitEvent.WaitOne();
        }
    }
}
