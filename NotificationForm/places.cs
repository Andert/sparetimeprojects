﻿using NotificationForm.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotificationForm
{
    class Places : Dictionary<string, Dictionary<string, string>>
    {
        public Places()
        {
            LoadResources();
        }
        private void LoadResources()
        {
            string[] data = Resources.noreg.Split(new string[] { "\r\n" }, StringSplitOptions.None);
            for (int i = 1; i < data.Length-1; i++)
            {
                string[] entries = data[i].Split('\t');
                string placeName = entries[1];
                if (!this.ContainsKey(placeName))
                {
                    this.Add(placeName, new Dictionary<string, string>());
                    string[] heads = data[0].Split('\t');
                    for (int j = 0; j < heads.Length && j < entries.Length; j++)
                    {
                        this[placeName].Add(heads[j], entries[j]);
                    }
                }
            }
        }
    }
}
