﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace NotificationForm
{
    public static class ForecastFetcher
    {

        public static List<float> GetForecast(string location)
        {

            XmlDocument bergenForecast = new XmlDocument();
            string url = "http://www.yr.no/sted/Norge/" + location + "varsel_nu.xml";
            bergenForecast.Load(url);
            XmlNode forecast = bergenForecast["weatherdata"]["forecast"];

            List<float> precipitation = new List<float>();

            foreach (XmlNode node in forecast.ChildNodes)
            {
                float pTemp = 0;
                float.TryParse(node["precipitation"].GetAttribute("value"), out pTemp);
                precipitation.Add(pTemp);
            }

            return precipitation;
        }

        public static bool IsRainingNext90Min(string location, List<float> forecast)
        {
            foreach (var item in forecast)
            {
                if (item > 0)
                {
                    return true;
                }
            }
            return false;
        }

        public static string GetStringForecast(string location, List<float> forecast)
        {
            string returnstring = "";
            for (int i = 1; i<forecast.Count; i+=2)
            {
                returnstring += $"Next {(i + 1) / 2 * 15} minutes: {forecast[i - 1] + forecast[i]}mm\r\n";
            }
            return returnstring;
        }
    }
}
