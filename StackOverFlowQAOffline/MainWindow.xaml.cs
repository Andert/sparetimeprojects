﻿using StackOverFlowQAOffline.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections;

namespace StackOverFlowQAOffline
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<string[]> data;
        public MainWindow()
        {
            string[] splitStrings = { "\r\n" };
            string[] splitStrings2 = { "\",\"" };
            string[] DataRows = Properties.Resources.QueryResults.Split(splitStrings,StringSplitOptions.None);
            data = new List<string[]>();
            foreach (var row in DataRows)
            {
                if (row.Split(splitStrings2, StringSplitOptions.None).Length == 3)
                {
                    data.Add(row.Split(splitStrings2, StringSplitOptions.None));
                }
            }
            InitializeComponent();
            //searchresult.ItemsSource = Data;
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox textBox = (TextBox)sender;
            // searchresult.ItemsSource = Data;
            if (textBox.Text.Length >= 3)
            {
                List<DataRow> searchResult = Search(textBox.Text);
                searchResultListView.ItemsSource = searchResult;
            }
        }

        private List<DataRow> Search(string text)
        {
            List<DataRow> searchResult = new List<DataRow>();
            foreach (var dataRow in data)
            {
                bool containsAllWords = true;
                foreach (var word in text.Split(' '))
                {
                    if (!dataRow[0].ToLower().Contains(word.ToLower()))
                    {
                        containsAllWords = false;
                        break;
                    }
                }
                if (containsAllWords)
                {
                    searchResult.Add(new DataRow(dataRow));
                }
            }
            return searchResult;
        }

        struct DataRow
        {
            public DataRow(string[] dataRow) : this()
            {
                if (dataRow.Length == 3)
                {
                    this.Title = dataRow[0];
                    this.Question = dataRow[1];
                    this.Answer = dataRow[2];
                }
            }

            public string Title { get; set; }
            public string Question { get; set; }
            public string Answer { get; set; }

            public override string ToString()
            {
                return Title;
            }
        }

        private void searchResultListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (searchResultListView.SelectedItem != null)
            {
                DataRow selectedDataRow = (DataRow)searchResultListView.SelectedItem;
                Question.NavigateToString(selectedDataRow.Question);
                Answer.NavigateToString(selectedDataRow.Answer);
            }
        }
    }
}
